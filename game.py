from random import randint

name = input('Hi! What is your name? ')
month_low = 1
month_high = 12
year_low = 1924
year_high = 2004
for guess_num in range(1, 6):
    month = randint(month_low, month_high)
    year = randint(year_low, year_high)
#range initialized with month_low etc so I can narrow down guessing parameters in future
    print('Guess', guess_num, ':', name, 'were you born in', month, '/', year, '?')

    answer = input('yes or no? ')

    if answer == 'yes':
        print('I knew it!')
        exit()
    elif guess_num == 5:
        print('This is a waste of time! Goodbye.')
    else:
        print('Drat! Lemme try again!')
